""" Reverse number """

num = input("Enter number: ")
tmp = int(num)

while tmp > 0:
	print(int(tmp) % 10)
	tmp = tmp // 10