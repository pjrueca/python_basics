board = []
rows = 3
cols = 3
slot = 9

# print board
def print_board():
	global board, rows, cols
	print("  1 2 3")

	for i in range(rows):
		print(i+1, end = "")
		
		for j in range(cols):
			print(" " + board[i][j], end = "")

		print()	
	print()

# init board
def init_board():
	global board, rows, cols

	for i in range(rows):
		board.insert(i, [])

		for j in range(cols):
			board[i].insert(i, "_")

# enter cell
def enter_cell():
	global board
	row = input("Enter row: ")
	row = int(row)-1
	col = input("Enter col: ")
	col = int(col)-1

	if board[row][col] != "_":
		return enter_cell()
	else:
		return { "row": row, "col": col }

# set cell value
def set_cell(mark):
	global slot
	cell = enter_cell();
	row = cell["row"]
	col = cell["col"]

	board[row][col] = mark
	slot -= 1

# check board
def check_board(mark):
	if board[1][1] == mark:
		if ((board[0][1] == mark and board[2][1] == mark) or
			(board[0][2] == mark and board[2][0] == mark) or
			(board[1][0] == mark and board[1][2] == mark) or
			(board[0][0] == mark and board[2][2] == mark)):
			return False

	if board[0][0] == mark:
		if ((board[0][1] == mark and board[0][2] == mark) or
			(board[1][0] == mark and board[2][0] == mark)):
			return False

	if board[2][2] == mark:
		if ((board[2][1] == mark and board[2][0] == mark) or
			(board[1][2] == mark and board[0][2] == mark)):
			return False

	return True

# game start
init_board()
print_board()
mark = "o"

while slot > 0:
	print("Player " + mark + "'s turn")
	set_cell(mark)
	print_board()

	if check_board(mark) == False:
		print("Player " + mark + " wins!")
		quit()

	if slot < 1:
		print("Game draw!")
		quit()
		
	mark = "x" if mark == "o" else "o"
