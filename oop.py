""" Python OOP """

class Skills:
	def __init__(self):
		super(Skills, self).__init__()
		self.skills = []


class Projects:
	def __init__(self):
		super(Projects, self).__init__()
		self.projects = {}


class Developer(Projects, Skills):
	def __init__(self, fname, lname):
		# Projects.__init__(self) for single inheritance
		super(Developer, self).__init__()
		self.fname = fname
		self.lname = lname

dev = Developer("Peter John", "Rueca")

dev.projects["APW"] = {
	"name": "Auto Parts Warehouse",
	"date": "June 28, 2016"
}

dev.projects["JCW"] = {
	"name": "JC Whitney",
	"date": "June 28, 2016"
}

dev.projects["CP"] = {
	"name": "Car Parts",
	"date": "June 28, 2016"
}

dev.projects["DBP"] = {
	"name": "Discount Body Parts",
	"date": "June 28, 2016"
}

dev.projects["CNC"] = {
	"name": "Comfort 'N Cure",
	"date": "February 15, 2018"
}

dev.skills.extend(["PHP", "JavaScript", "jQuery", "MySQL", "HTML5", "CSS3"])

print(dev.skills)
print(dev.projects)